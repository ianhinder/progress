// #include <string>
#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Schedule.h"
// #include <iostream>
#include <math.h>
#include <string.h>
#include <iostream>

using namespace std;

extern "C" {

void Progress_Calculate(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // TODO: consider actual termination condition specified for flesh
  // such as cctk_itlast rather than assuming cctk_final_time

  assert(final_time);
  assert(fraction_complete);

  *final_time = cctk_final_time;
  *fraction_complete = fmin(cctk_time/ (*final_time - cctk_initial_time),1);
  *percent_complete = 100* *fraction_complete;

  CCTK_REAL *physical_time_per_hour = (CCTK_REAL *) CCTK_VarDataPtr(cctkGH, 0, "Carpet::physical_time_per_hour");

  *speed = (physical_time_per_hour && cctk_iteration != 0 && *physical_time_per_hour > 0) ? *physical_time_per_hour : -1;
  *remaining_walltime = (*speed != -1) ? ((1-*fraction_complete)*(*final_time - cctk_initial_time) / *speed) : -1;

  // cout << "physical_time_per_hour = " << *physical_time_per_hour << endl;
  // cout << "speed = " << *speed << endl;
  // cout << "remaining_walltime = " << *remaining_walltime << endl;
}

void Progress_DisplayPeriodically(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (out_every != -1 && (cctk_iteration % out_every == 0) && cctk_iteration != 0) {
    Progress_Display(cctkGH);
  }
}

void Progress_Display(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

    assert(final_time);
    assert(fraction_complete);

    // char time_left[100];

    // struct tm tm_time_left;

    // tm_time_left.;

    // strftime(time_left, sizeof(time_left), "%X", &tm_time_left);

    // *remaining_walltime;

    const int second = 1;
    const int minute = 60*second;
    const int hour   = 60*minute;
    const int day    = 24*hour;

    // Seconds remaining
    int tleft = *remaining_walltime * 3600;

    int days = tleft/day;
    int hours = (tleft % day) / hour;
    int minutes = (tleft % hour) / minute;
    int seconds = (tleft % minute) / second;

    char time_finish[] = "unknown";
    char date_finish[] = "unknown";

    char remtime_str[256];

    // cout << "display: remaining_walltime = " << *remaining_walltime << endl;

    if (*remaining_walltime != -1) {
      sprintf(remtime_str, "%dd %.2dh %.2dm %.2ds", days, hours, minutes, seconds);
    }
    else {
      sprintf(remtime_str, "unknown");
    }

    CCTK_VInfo(CCTK_THORNSTRING,
               "%.2f%% complete, total time remaining: %s",
               *percent_complete, remtime_str);

    // std::stringstream ss;
    // ss << "igprof." << CCTK_MyProc(cctkGH) << "." << cctk_iteration;
    // dump_(ss.str().c_str());
}

}
